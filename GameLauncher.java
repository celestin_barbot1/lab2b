import java.util.Scanner;

public class GameLauncher{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome! Which game do you want to play right now?\n Enter the number for your choise\n1 -Wordle \n2 -Hangman");
		int gameChoice = reader.nextInt();
		
		if (gameChoice == 1){
			// Wordle
			
			Wordle.runGame(Wordle.generateWord());
		} else if (gameChoice == 2){
			// Hangman
			Hangman.runGame();
		} else {
			System.out.println("Invalid choice, try 1 for Wordle, 2 for Hangman...");
		}
	}
}