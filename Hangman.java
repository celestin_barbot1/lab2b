import java.util.Scanner;


public class Hangman{

    // This function compares a character from the user to the word he's seeking.
    public static int isLetterInWord(String word, char c){
        /* Variable 'i' is initialed and later incremented in the loop, 
        it's function is to count through the word and then put an end to the loop. E
        Effectively serves as an index.
        */
        int i = 0;
        /* This loop iterates through each letter in the word, comparing it to the character chosen by the user. 
        When a match is found, it returns the index of the matching character. 
        If no match found returns '-1'*/
        while (i<=3) {
            if (toUpperCase(word.charAt(i)) == c){
                return i;
            }
            i++;
        }
        return -1;
    }

    /* This function checks if a character is lowercase by comparing it's number on the unicode table. 
    If it is subtracts 32 from it's number to make it uppercase. If it's already uppercase, it's returned as is.
     */
    public static char toUpperCase(char c) {
        if(c >= 97 && c <= 122){
            c= (char) (c-32);
            return(c);
        }else {
            return(c);
        } 

    /*
     This function is employed to display the game's result. It takes 5 parameters from the 
     'runGame' function
     It initializes four character variables, labeled 0 to 3, 
     where each variable corresponds to one of the four letters in the word to be found. 
     It subsequently checks if the boolean variable associated with the letter in the word to be found is set to true. 
     If it is, the function updates one of the variables to match the word's letter.
     The functions ends up printing the game's result.
     */
    }
    public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
        char result0 = '_';
        char result1 = '_';
        char result2 = '_';
        char result3 = '_';
        
        if (letter0) {
            result0 = word.charAt(0);
        }
        if (letter1) {
            result1 = word.charAt(1);
        }
        if (letter2) {
            result2 = word.charAt(2);
        }
        if (letter3) {
            result3 = word.charAt(3);
        } 

        String finalResult = "Your result is " + result0 + result1 + result2 + result3;
        System.out.println(finalResult);
        


        /*
         This function is the one that contains the actual gameplay, it initializes 4 boolean variable and sets them to false.
         Uses a loop (loop condition comment right above it) to ask the user for a letter and takes the input using a scanner object initialized at the beginning of the function.
         The character is set to uppercase using the 'toUpperCase' function.
         then, the character is compared to the characters of the word using the 'isLetterInWord'
         if it matches the boolean variable corresponding to the index of the guessed letter is set to true.
         If it doesnt matches any letter of the word the variable 'playerMisses' is decremented by 1
         The function ends up calling the 'printWord' function.
         */
    }

    public static void runGame(){
		java.util.Scanner reader = new java.util.Scanner(System.in);
        System.out.println("What word do you want to play with? \n(4 letter word where all letters are uppercase and different): ");
        String userWordInput = reader.nextLine();

      
      

        boolean letter0 = false;
        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;
        int playerMisses = 6;
        
        /*  The condition for this loop firsts check if the player has any chances left, if he does
        checks if one of the letter of the word still has to be guessed.
        */
        while (playerMisses > 0 && (letter0 != true || letter1 != true || letter2 != true || letter3 != true)){

            System.out.println("Enter a letter:");
            char userCharacterInput = toUpperCase(reader.nextLine().charAt(0));

            if (isLetterInWord(userWordInput, userCharacterInput) == 0) {
                letter0 = true;
            } else if (isLetterInWord(userWordInput, userCharacterInput) == 1) {
                letter1 = true;
            } else if (isLetterInWord(userWordInput, userCharacterInput) == 2) {
                letter2 = true;
            } else if (isLetterInWord(userWordInput, userCharacterInput) == 3) {
                letter3 = true;
            } else {
                playerMisses--;
                System.out.println("Miss! " + playerMisses +" chances left...");
            }
            printWord(userWordInput, letter0, letter1, letter2, letter3);
        }
        if(playerMisses == 0){
            System.out.println("Better luck next time.");
        } else if (letter0 == true && letter1 == true && letter2 == true && letter3 == true){
            System.out.println("You win!");
        }
        
    }
}